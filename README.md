##Bulk insertion with walk down

This repository consists of the code  used in my master thesis. 
All data used to make the plots in my thesis is generated from this code. 
The data for the plots for the random testcase can be found in the folder _RandomResults_ and the data for the plots for the worst-case scenario can be found inside the _WorstCaseResults_ folder.
The process of reproducing these results is described at the bottom of this page.

_Bulk insertion with walk down_ is an _k_-insertion algorithm for binary heaps. 
The algorithm first adds the _k_ new element to the end of the heap in a bulk manner. 
Then it finds two nodes _p_ and _q_ covering the _k_ new elements. 
Then it performs the procedure _heapify_ on the two subheaps rooted at _p_ and _q_. 
Lastly it fixes the broken heap property between _p_ and _q_ and their parent through a procedure called _walk_ _down_. 
This algorithm is more thorouchly described in Chapter 4 in the thesis.

The algorithm runs in worst case time _O(k + log(k) * log((n + k) / k))_, where _n_ is the size of the initial heap and _k_ are
the number of elements added to the heap.

In this project we implement the _k_-insertion method _Bulk insertion with walk down_ and compare its practical running time to the _k_-insertion algorithms _Sequential insertion_ and _Bulk insertion with heapify_.
As Javas _PriorityQueue.java_ class is implemented using a binary heap, all implementations are based upon this class. Also, the method _addAll_ in the _PriorityQueue.java_ class adds a collection of elements to the heap.
Thus, for each algorithm the _addAll_ method is changed according to the rules of the algorithm. 

The implementation of each alogorithm can be found in the _heap_ folder. The implementation for _Bulk insertion with walk down_ can be found in the _BulkInsertionWalkDown.java_ class 
The implementation for _Bulk insertion with heapify_ can be found in the _BulkInsertionHeapify.java_ class. 
The implementation for _Sequential insertion_ can be found in the _PriorityQueue.java_ class inside the _heap_ folder. 
As Javas Priority queue class is implemented using the sequential insertion method this class is a direct copy of that. 

There is also implemented JUnit tests for the algorithms. This can be found in the _test_ folder. 
The tests are organized as follows; 
First there is a superclass _HeapTest.java_ that both the testclasses for the bulk insertion methods extends.
The class _HeapTest.java_ test the _PriorityQueue.java_ class, while the classes _BulkInsertionWalkDown.java_ and _BulkInsertionHeapify_ tests the  _Bulk insertion with walk down_ and the _Bulk insertion with heapify_ algorithms. 

Lastly, in the _Main.Java_ file we conduct benchmark testing for the three algorithms. 
This file was used to make the data for the plots in my master thesis. 
This class consist of one function to run the algorithms on random data and one function to run the algorithms on worst-case data. 
Each of these two functions time the algorithms on a specified amount of datasets and save the results. 

To recreate the data used to construct the plot in the thesis one can run _Main.java_:
To run the algorithms on random data one need to make sure that code on line 214 is not commented out while the code on line 213 is commented out. 
Conversly, to run the algorithms on the worst-case data one need to make sure that the code on line 213 is not commented out and the code on line 214 is commented out. 
The program stores the results for each algorithm in the files _PriorityQueueResults.tx_, _WalkDownResults.txt_ and _HeapifyResults.txt_. 
To prevent the files being overwritten between running two consecutive testcase one needs to move the resultfiles before running the second testcase. 
On top of the _Main.java_ class there are a numerous amount of parameters one can change to generate different results. 

- The parameter _numIteration_ represents the number of datasets the chosen testcase should use.

- The parameter _n_ represents the number of elements initially in the heaps before the _k_-insertion algorithms are applied.

- The parameter _k_ represents the number of elements the _k_-insertion algorithms adds to the heap. 

- The parameter _runWithTests_ can either be true of false and here one can decide if one wants to run tests that check if the _Bulk insertion with heapify_ performs correctly. 
