import heap.BulkInsertionWalkDown;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BulkInsertionWalkDownTest extends HeapTest {

	@Override
	protected Queue<Integer> getHeap() {
		return new BulkInsertionWalkDown<>();
	}

	@Test
	void testSingleLeftUninitializedWalkDownSmallValues() {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i < 31
				; i++)
			heap.add(10);
		for (int i = 0; i < 4; i++)
			data.add(i);
		heap.addAll(data);
		Object[] heapList = heap.toArray();
		assertEquals(0, heapList[0]);
		assertEquals(1, heapList[1]);
		assertEquals(2, heapList[3]);
	}

	@Test
	void testSingleRightUninitializedWalkDownSmallValues() {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i < 55; i++)
			this.heap.add(10);
		for (int i = 0; i < 3; i++)
			data.add(i);
		this.heap.addAll(data);
		Object[] heapList = heap.toArray();

		assertEquals(0, heapList[0]);
		assertEquals(1, heapList[2]);
		assertEquals(2, heapList[6]);
	}

	@Test
	void testSingleLeftUninitializedWalkDownMixedValues() {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i < 31; i++)
			this.heap.add(10);
		data.add(8);
		data.add(11);
		data.add(0);
		this.heap.addAll(data);
		Object[] heapList = heap.toArray();
		assertEquals(0, heapList[0]);
		assertEquals(8, heapList[1]);
		assertEquals(10, heapList[3]);
	}

	@Test
	void testSingleRightUninitializedWalkDownMixedValues() {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i < 55; i++)
			this.heap.add(10);
		data.add(8);
		data.add(11);
		data.add(0);
		this.heap.addAll(data);
		Object[] heapList = heap.toArray();assertEquals(0, heapList[0]);
		assertEquals(8, heapList[2]);
		assertEquals(10, heapList[6]);
	}

	@Test
	void testSingleWalkDownMiddleOfHeap() {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i < 47; i++)
			this.heap.add(10);
		for (int i = 0; i < 4; i++)
			data.add(i);
		this.heap.addAll(data);
		Object[] heapList = heap.toArray();
		assertEquals(0, heapList[0]);
		assertEquals(1, heapList[2]);
		assertEquals(2, heapList[5]);
		assertEquals(3, heapList[11]);
	}

	@Test
	void testHeapifyIndex() {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i < 15; i++)
			this.heap.add(i);
		data.add(5);
		data.add(4);
		data.add(2);
		data.add(6);
		this.heap.addAll(data);
		Object[] heapList = heap.toArray();
		assertEquals(2, heapList[3]);
		assertEquals(4, heapList[7]);
		assertEquals(3, heapList[8]);
		assertEquals(5, heapList[15]);
		assertEquals(7, heapList[16]);
		assertEquals(8, heapList[17]);
		assertEquals(6, heapList[18]);
	}

	@Test
	void testAddElemLargerThanRootSmallerThanParent() {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i <= 11; i++) {
			this.heap.add(i);
			data.add(i);
		}

		this.heap.addAll(Arrays.asList(1));
		data.add(1);
		Collections.sort(data);
		for(int elem : data) {
			assertEquals(elem, this.heap.poll());
		}

	}


}
