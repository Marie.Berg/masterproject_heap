
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class HeapTest {
	protected Queue<Integer> heap;

	@BeforeEach
	void init() {
		this.heap = getHeap();
	}

	boolean checkHeapInvariant(){
		Object[] heapArray = this.heap.toArray();
		int n = heapArray.length - 1, i = (n >>> 1) - 1;
		for(; i >= 0; i--) {
			int parent = (int) heapArray[i];
			int leftChild = (int) heapArray[(i << 1) + 1];
			int rightChild = (int) heapArray[(i << 1) + 2];
			if(parent > leftChild || parent > rightChild)
				return false;
		}
		return true;
	}

	protected Queue<Integer> getHeap() {
		return new PriorityQueue<>();
	}

	//TESTING CONSTRUCTOR

	@Test
	void testEmptyConstructor(){
		assertTrue(this.heap.isEmpty());
		assertEquals(0, this.heap.size());
	}

	@Test
	void testEmptyHeapWhenInitialCapasityConstructor(){

	}

	//TESTING SIZE

	@Test
	void testSizeOnEmptyHeap(){
		assertEquals(0, this.heap.size());
	}

	@Test
	void testSizeAfterAdd() {
		this.heap.add(1);
		assertEquals(1, this.heap.size());
		this.heap.add(2);
		assertEquals(2, this.heap.size());
		this.heap.add(3);
		assertEquals(3, this.heap.size());
		this.heap.add(4);
		assertEquals(4, this.heap.size());
		this.heap.add(7);
		assertEquals(5, this.heap.size());
	}

	@Test
	void testSizeWhenAddingSmallerAfterLarger(){
		this.heap.add(5);
		this.heap.add(2);
		assertEquals(2, this.heap.size());
	}

	@Test
	void testSizeAfterDuplicateAdd(){
		this.heap.add(7);
		assertEquals(1, this.heap.size());
		this.heap.add(7);
		assertEquals(2, this.heap.size());
		this.heap.add(7);
		assertEquals(3, this.heap.size());
		this.heap.add(3);
		assertEquals(4, this.heap.size());
	}

	@Test
	void testSizeAfterRemove() {
		this.heap.add(1);
		this.heap.add(2);
		this.heap.remove(1);
		assertEquals(1, heap.size());
	}

	@Test
	void testSizeAfterAddAll() {
		ArrayList<Integer> input = new ArrayList<Integer>();
		input.add(1);
		input.add(2);
		input.add(3);
		this.heap.addAll(input);
		assertEquals(3, this.heap.size());
	}

	@Test
	void testSizeAfterRemoveAll() {
		ArrayList<Integer> removeData = new ArrayList<Integer>();
		removeData.add(2);
		removeData.add(4);
		removeData.add(1);
		removeData.add(0);

		this.heap.add(1);
		this.heap.add(2);
		this.heap.add(3);
		this.heap.add(4);
		this.heap.removeAll(removeData);

		assertEquals(1, this.heap.size());
	}

	@Test
	void testSizeAfterRemovingElementsNotInTheHeap(){
		ArrayList<Integer> removeData = new ArrayList<Integer>();
		removeData.add(2);
		removeData.add(9); //Not contained in the heap
		removeData.add(5); //Not contained in the heap

		this.heap.add(1);
		this.heap.add(2);
		this.heap.add(3);
		this.heap.add(4);
		this.heap.removeAll(removeData);

		assertEquals(3, this.heap.size());
	}

	@Test
	void testSizeAfterRetainAll(){
	}

	//TESTING ISEMPTY()

	@Test
	void testIsEmptyOnEmptyHeap(){
		assertTrue(this.heap.isEmpty());
	}

	@Test
	void testIsEmptyWhenNotEmpty(){
		this.heap.add(1);
		assertFalse(this.heap.isEmpty());
		this.heap.add(2);
		assertFalse(this.heap.isEmpty());
	}

	//TESTING REMOVEIF

	@Test
	void testRemoveIf(){
		//	assertTrue(false);
	}

	//TESTING REMOVE SPECIFIC ELEMENT

	@Test
	void testRemoveSpecificElementRemovesElement(){
		this.heap.add(1);
		this.heap.add(4);
		this.heap.add(5);
		this.heap.add(8);
		this.heap.add(9);
		this.heap.add(2);
		assertTrue(this.heap.remove(4));
		assertFalse(this.heap.contains(4));
	}

	@Test
	void testRemoveSpecificElementReturnsFalseWhenRemovingElementNotInTheHeap(){
		this.heap.add(1);
		this.heap.add(4);
		this.heap.add(5);
		this.heap.add(8);
		this.heap.add(9);
		this.heap.add(2);
		this.heap.add(4);

		assertFalse(this.heap.remove(0));
		assertFalse(this.heap.remove(11));
	}

	@Test
	void testRemovingSpecificElementsNotInTheHeap(){
		this.heap.add(1);
		this.heap.add(4);
		this.heap.add(5);
		this.heap.add(8);
		this.heap.add(9);
		this.heap.add(2);

		assertFalse(this.heap.remove(0));
		assertFalse(this.heap.remove(11));
		assertFalse(this.heap.remove(3));
		assertTrue(this.heap.contains(1));
		assertTrue(this.heap.contains(4));
		assertTrue(this.heap.contains(5));
		assertTrue(this.heap.contains(8));
		assertTrue(this.heap.contains(9));
		assertTrue(this.heap.contains(2));
	}

	@Test
	void testRemoveSpecificElementsDuplicateElements(){
		this.heap.add(1);
		this.heap.add(4);
		this.heap.add(5);
		this.heap.add(8);
		this.heap.add(9);
		this.heap.add(2);
		this.heap.add(4);

		assertTrue(this.heap.remove(4));
		assertTrue(this.heap.contains(4));
	}

	@Test
	void testRemoveSpecificElementPreservesHeapInvariant(){
		this.heap.add(1);
		this.heap.add(3);
		this.heap.add(5);
		this.heap.add(9);
		this.heap.add(4);
		this.heap.add(7);
		this.heap.add(11);
		this.heap.add(22);
		this.heap.add(13);
		this.heap.add(20);
		assertTrue(this.heap.remove(3));
		assertTrue(checkHeapInvariant(), "Parent is larger than child");
		assertTrue(this.heap.remove(11));
		assertTrue(checkHeapInvariant(), "Parent is larger than child");
		assertFalse(this.heap.remove(12), "The heap whas modified");
		assertTrue(checkHeapInvariant(), "Parent is larger than child");

	}

	//TESTING REMOVEALL()

	@Test
	void testRemoveAll() {
		this.heap.add(1);
		this.heap.add(4);
		this.heap.add(5);
		this.heap.add(8);
		this.heap.add(9);
		this.heap.add(2);
		this.heap.add(4);
		this.heap.add(11);
		this.heap.add(10);
		this.heap.add(19);
		this.heap.add(22);

		ArrayList<Integer> removeData = new ArrayList<>();
		removeData.add(3);
		removeData.add(22);
		removeData.add(8);
		removeData.add(7);

		assertTrue(this.heap.removeAll(removeData));
		assertTrue(this.heap.contains(1));
		assertTrue(this.heap.contains(4));
		assertTrue(this.heap.contains(5));
		assertFalse(this.heap.contains(8));
		assertTrue(this.heap.contains(9));
		assertTrue(this.heap.contains(2));
		assertTrue(this.heap.contains(11));
		assertTrue(this.heap.contains(10));
		assertTrue(this.heap.contains(19));
		assertFalse(this.heap.contains(22));
		assertFalse(this.heap.contains(7));
		assertFalse(this.heap.contains(3));
	}


	@Test
	void testRemoveAllReturnsFalseWhenRemovingElementsNotInTheHeap(){
		ArrayList<Integer> removeData = new ArrayList<>();
		removeData.add(3);
		removeData.add(22);
		removeData.add(17);

		this.heap.add(1);
		this.heap.add(4);
		this.heap.add(5);
		this.heap.add(8);
		this.heap.add(9);
		this.heap.add(2);
		this.heap.add(4);
		this.heap.add(11);
		this.heap.add(19);

		assertFalse(this.heap.removeAll(removeData));
		assertTrue(this.heap.contains(1));
		assertTrue(this.heap.contains(4));
		assertTrue(this.heap.contains(5));
		assertTrue(this.heap.contains(8));
		assertTrue(this.heap.contains(9));
		assertTrue(this.heap.contains(2));
		assertTrue(this.heap.contains(4));
		assertTrue(this.heap.contains(11));
		assertTrue(this.heap.contains(19));
	}


	@Test
	void testRemoveAllPreservesHeapInvariant(){
		ArrayList<Integer> removeData = new ArrayList<>();
		removeData.add(3);
		removeData.add(7);
		removeData.add(22);

		this.heap.add(1);
		this.heap.add(3);
		this.heap.add(5);
		this.heap.add(9);
		this.heap.add(4);
		this.heap.add(7);
		this.heap.add(11);
		this.heap.add(22);
		this.heap.add(13);
		this.heap.add(20);
		this.heap.add(8);
		this.heap.add(12);
		this.heap.add(17);
		this.heap.add(12);
		this.heap.add(30);
		this.heap.add(23);
		this.heap.add(15);

		assertTrue(this.heap.removeAll(removeData));
		assertTrue(checkHeapInvariant(), "Parent is larger than child");
	}

	//TESTING RETAINALL()

	@Test
	void testRetainAll(){
		ArrayList<Integer> retainData = new ArrayList<>();
		retainData.add(12);
		retainData.add(9);
		retainData.add(30);
		retainData.add(1);
		retainData.add(4);

		this.heap.add(1);
		this.heap.add(3);
		this.heap.add(5);
		this.heap.add(9);
		this.heap.add(4);
		this.heap.add(7);
		this.heap.add(11);
		this.heap.add(22);
		this.heap.add(13);
		this.heap.add(20);
		this.heap.add(8);
		this.heap.add(12);
		this.heap.add(17);
		this.heap.add(30);
		this.heap.add(23);
		this.heap.add(15);

		assertTrue(this.heap.retainAll(retainData));
		assertEquals( retainData.size(), this.heap.size());
		assertTrue(this.heap.containsAll(retainData));
		assertFalse(this.heap.contains(3));
		assertFalse(this.heap.contains(5));
		assertFalse(this.heap.contains(7));
		assertFalse(this.heap.contains(11));
		assertFalse(this.heap.contains(22));
		assertFalse(this.heap.contains(13));
		assertFalse(this.heap.contains(20));
		assertFalse(this.heap.contains(8));
		assertFalse(this.heap.contains(17));
		assertFalse(this.heap.contains(23));
		assertFalse(this.heap.contains(15));
	}


	@Test
	void testRetainAllPreservesHeapInvariant() {
		ArrayList<Integer> retainData = new ArrayList<>();
		retainData.add(12);
		retainData.add(9);
		retainData.add(30);
		retainData.add(1);
		retainData.add(4);

		this.heap.add(1);
		this.heap.add(3);
		this.heap.add(5);
		this.heap.add(9);
		this.heap.add(4);
		this.heap.add(7);
		this.heap.add(11);
		this.heap.add(22);
		this.heap.add(13);
		this.heap.add(20);
		this.heap.add(8);
		this.heap.add(12);
		this.heap.add(17);
		this.heap.add(12);
		this.heap.add(30);
		this.heap.add(23);
		this.heap.add(15);

		assertTrue(this.heap.retainAll(retainData));
		assertTrue(checkHeapInvariant());
	}

	//TESTING CONTAINS

	@Test
	void testContainsIsTrue() {
		this.heap.add(1);
		this.heap.add(5);
		this.heap.add(6);

		assertTrue(this.heap.contains(1));
		assertTrue(this.heap.contains(5));
		assertTrue(this.heap.contains(6));
	}

	@Test
	void testContainsIsFalse() {
		this.heap.add(2);
		assertFalse(this.heap.contains(1));
	}

	//TESTING CLEAR

	@Test
	void testClear() {
		this.heap.add(1);
		this.heap.add(10);
		this.heap.add(3100);
		this.heap.add(131);
		this.heap.add(2);
		this.heap.add(11);
		this.heap.add(24);
		this.heap.add(43);
		this.heap.add(28);
		this.heap.clear();
		assertTrue(this.heap.isEmpty());
	}

	//TESTING ADD

	@Test
	void testAddNullThrowsException() {
		this.heap.add(1);
		assertThrows(NullPointerException.class, () -> this.heap.add(null));
	}

	@Test
	void testAddPreservesHeapInvariant() {
		this.heap.add(4);
		assertTrue(checkHeapInvariant());
		this.heap.add(5);
		assertTrue(checkHeapInvariant());
		this.heap.add(7);
		assertTrue(checkHeapInvariant());
		this.heap.add(1);
		assertTrue(checkHeapInvariant());
		this.heap.add(8);
		assertTrue(checkHeapInvariant());
		this.heap.add(0);
		assertTrue(checkHeapInvariant());
		this.heap.add(-1);
		assertTrue(checkHeapInvariant());
	}

	//TESTING PEEK

	@Test
	void testPeek() {
		this.heap.add(4);
		assertEquals(4, this.heap.peek());
		this.heap.add(3);
		assertEquals(3, this.heap.peek());
		this.heap.add(4);
		assertEquals(3, this.heap.peek());
		this.heap.add(2);
		assertEquals(2, this.heap.peek());
		this.heap.add(1);
		assertEquals(1, this.heap.peek());
	}

	//TESTING POLL

	@Test
	void testPollFromEmptyHeapRetunsNull() {
		assertNull(this.heap.poll());
		this.heap.add(1);
		this.heap.poll();
		assertNull(this.heap.poll());
	}

	@Test
	void testSimplePoll() {
		this.heap.add(1);
		assertEquals(1, this.heap.poll());
		this.heap.add(0);
		assertEquals(0, this.heap.poll());
		this.heap.add(5);
		assertEquals(5, this.heap.poll());
	}

	@Test
	void testPoll() {
		this.heap.add(4);
		this.heap.add(7);
		this.heap.add(2);
		assertEquals(2, this.heap.poll());
		this.heap.add(9);
		this.heap.add(6);
		assertEquals(4, this.heap.poll());
		this.heap.add(6);
		this.heap.add(6);
		assertEquals(6, this.heap.poll());
		assertEquals(6, this.heap.poll());
	}

	//TESTING REOMOVE()

	@Test
	void testRemoveOnEmptyHeapThrowsException() {
		assertThrows(NoSuchElementException.class, () -> this.heap.remove());
	}

	@Test
	void testRemove(){
		this.heap.add(4);
		this.heap.add(7);
		this.heap.add(2);
		assertEquals(2, this.heap.remove());
		this.heap.add(9);
		this.heap.add(6);
		assertEquals(4, this.heap.remove());
		this.heap.add(6);
		this.heap.add(6);
		assertEquals(6, this.heap.remove());
		assertEquals(6, this.heap.remove());
	}

	//TESTING PEEK

	@Test
	void testPeekReturnsNull() {
		assertNull(this.heap.peek());
	}

	//TESTING ELEMENT()

	@Test
	void testElement(){
		this.heap.add(4);
		assertEquals(4, this.heap.element());
		this.heap.add(3);
		assertEquals(3, this.heap.element());
		this.heap.add(4);
		assertEquals(3, this.heap.element());
		this.heap.add(2);
		assertEquals(2, this.heap.element());
		this.heap.add(1);
		assertEquals(1, this.heap.element());
	}

	@Test
	void testElementThrowsException(){
		assertThrows(NoSuchElementException.class, () -> this.heap.element());
	}

	//TESTING ADDALL

	@Test
	void testAddAll() {
		heap.add(3);
		heap.add(6);
		heap.add(1);
		heap.add(4);
		ArrayList<Integer> input = new ArrayList<>();
		input.add(2);
		input.add(7);
		input.add(5);
		input.add(8);
		input.add(9);
		input.add(0);
		heap.addAll(input);
		for(int i=0; i<10; i++){
			assertEquals(i, heap.poll());
		}
	}

	@Test
	void testAddAllOnEmptyHeap() {
		ArrayList<Integer> input = new ArrayList<Integer>();
		input.add(1);
		input.add(2);
		input.add(3);
		this.heap.addAll(input);
		Collections.sort(input);
		for(Integer elem : input) {
			assertEquals(elem, heap.poll());
		}
	}

	@Test
	void testAddAllLargeValuesOnSmallValuedHeap(){
		List<Integer> small = Arrays.asList(10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
		List<Integer> large = Arrays.asList(20, 35, 17, 22, 40, 87, 72, 33, 67, 15);
		List<Integer> all = new ArrayList<>();

		for(int elem : small){
			heap.add(elem);
		}

		heap.addAll(large);

		all.addAll(small);
		all.addAll(large);
		Collections.sort(all);

		assertEquals(all.size(), heap.size());

		for(int elem: all){
			assertEquals(elem, heap.poll());
		}
	}

	@Test
	void testAddAllSmallValuesOnLargeValueHeap(){
		List<Integer> small = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		List<Integer> large = Arrays.asList(20, 35, 17, 22, 40, 87, 72, 33, 67, 15);
		List<Integer> all = new ArrayList<>();

		for(int elem : large) {
			heap.add(elem);
		}

		heap.addAll(small);

		all.addAll(small);
		all.addAll(large);
		Collections.sort(all);

		assertEquals(all.size(), heap.size());

		for(int elem: all){
			assertEquals(elem, heap.poll());
		}
	}

	@Test
	void testAddAllEqualElements(){
		List<Integer> before = Arrays.asList(3, 5, 6, 7, 2, 4, 1);
		List<Integer> input = Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4);

		List<Integer> all = new ArrayList<>();
		all.addAll(before);
		all.addAll(input);
		Collections.sort(all);

		for(int elem : before) {
			heap.add(elem);
		}

		heap.addAll(input);

		assertEquals(all.size(), heap.size());
		for(int elem : all) {
			assertEquals(elem, heap.poll());
		}
	}

	@Test
	void testAddAllOnEqualValueHeap(){
		List<Integer> input = Arrays.asList(3, 5, 6, 7, 2, 4, 1);
		List<Integer> before = Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4);
		List<Integer> all = new ArrayList<>();
		all.addAll(before);
		all.addAll(input);
		Collections.sort(all);
		for(int elem : before) {
			heap.add(elem);
		}
		heap.addAll(input);
		assertEquals(all.size(), heap.size());
		for(int elem : all) {
			assertEquals(elem, heap.poll());
		}
	}

	@Test
	void testAddAllOnEmptyList(){

	}

	@Test
	void testAddAllParentOfHiLessThanLoAndHiLeftChild(){
		for(int i=0; i<6;i++)
			heap.add(2);
		ArrayList<Integer> input = new ArrayList<>();
		input.add(1);
		for(int i=7; i<13; i++)
			input.add(2);
		input.add(3);
		for(int i=14; i<27; i++)
			input.add(2);
		input.add(3);
		heap.addAll(input);
		assertEquals(1, heap.poll());
	}

	@Test
	void testAddAllOnLargeHeap(){
		System.out.println("Large");
		List<Integer> all = new ArrayList<>();
		for(int i=0; i < 200; i++) {
			heap.add(200);
			all.add(200);
		}
		List<Integer> add = new ArrayList<>();
		for(int i=25; i > 0; i--) {
			add.add(i);
		}
		all.addAll(add);
		heap.addAll(add);
		Collections.sort(all);
		for(int e : all){
			assertEquals(e, heap.poll());
		}
	}
}