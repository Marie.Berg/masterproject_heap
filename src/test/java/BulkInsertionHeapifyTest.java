import heap.BulkInsertionHeapify;

import java.util.Queue;

public class BulkInsertionHeapifyTest extends HeapTest {

    @Override
    protected Queue<Integer> getHeap() {
        return new BulkInsertionHeapify<>();
    }
}
