package heap;

import java.util.*;

/**
 * A prioriry queue similar to {@link PriorityQueue} where the {@Code addAll} method combined the tree subroutines
 * findTwoNodes, heapifySubheap and walkDown. The procedure findTwoNodes finds two nodes p and q that covers the new elements added to the heap.
 * The procedure heapifySubheap performs the procedure heapify on the subheap rooted at the input index.
 * The procedure walkDown restores the heap propery of the entire heap when it is broken between a node and its parent.
 *
 * @param <E>
 */
public class BulkInsertionWalkDown<E> extends PriorityQueue<E> {

    private static final long serialVersionUID = -3580419020864597085L;
    static private transient Object[] tempValues = new Object[64];


    /**
     * Private method for inserting a collection of elements to the end of the array holding the heap
     * @param c the colection of element to be added
     * @return {@code true} if this queue changed as a result of the call
     */
    private boolean insert(Collection<? extends E> c) {
        boolean modified = false;

        if(isEmpty()) {
            initElementsFromCollection(c);
            modified = !c.isEmpty();
        }
        else {
            Object[] objs = c.toArray();
            int len = objs.length;
            if (objs.getClass() != Object[].class)
                objs = Arrays.copyOf(objs, len, Object[].class);
            for(Object e : objs){
                if (e == null)
                    throw new NullPointerException();
                modified = true;
            }
            System.arraycopy(objs, 0, queue, size(), len);
            this.size += len;
        }
        modCount++;
        return modified;
    }

    /***
     * @param n
     * @return the height of a binary heap of size n
     */
    private int getHeight(int n) {
        return (int) Math.floor(Math.log(n + 1) / Math.log(2));
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int k; //Number of elements added
        int p; //p_l
        int q; //p_r
        int h; //Height of p_l and p_r
        if (c == null)
            throw new NullPointerException();
        if (c == this)
            throw new IllegalArgumentException();
        if ((k = c.size()) == 0)
            return false;

        if ((p = size) >= queue.length - k)
            grow(p + k + 1);

        boolean modified = insert(c);

        if(k >= p / 2){
            heapify();
            return modified;
        }
        h = getHeight(k);
        q = size - 1;

        int pp = p;
        int qq = q;

        //Finds p_l and p_r
        for(int i = 0; i <= h; i++){
            boolean updateP = (p % 2 == 1);
            boolean updateQ = (q % 2 == 0);
            p = (p - 1) >>> 1;
            q = (q - 1) >>> 1;
            if(updateP)
                pp = p;
            if(updateQ)
                qq = q;
        }

        p=pp;
        q=qq;

        if (p == q) { //Hit least common ancestor of p_l and p_r
            heapify(p);
            walkDown(p);
        } if(q % 2== 0 && q == p+1) { //Siblings
            heapify((p - 1) >>> 1);
            walkDown((p - 1) >>> 1);
        } else {
            heapify(p);
            walkDown(p);
            heapify(q);
            walkDown(q);
        }
        return modified;
    }

    /***
     * Performs the procedure Heapify on a subheap rooted at a specific node
     * @param idx  Index of the root of the subheap
     */
    private void heapify(int idx) {
        int left = idx, right = idx;
        int half = (size-2) >>> 1;
        while (right <= half) {
            left = (left << 1) + 1;
            right = (right << 1) + 2;
        }

        int end = Math.min(half, right);

        for(int i = left; i <= end; i++) //First layer might be incomplete
            siftDown(i, (E) queue[i]);

        while(right > idx) {
            left = (left - 1) >>> 1;
            right = (right - 1) >>> 1;
            for(int i = left; i <= right; i++)
                siftDown(i, (E) queue[i]);
        }
    }

    /**
     * Performs the procedure walk down on the subheap rooted at the input index.
     * Restores the heap property of the entire heap when it is borken between the node at index i and its parent
     * @param i
     */
    public void walkDown(int i) {
        if(i == 0)
            return;
        if(comparator != null) {
            WalkDownUsingComparator(i, queue, comparator);
        } else {
            WalkDownComparable(i, queue);
        }
    }

    private void WalkDownComparable(int i, Object[] es) {
        List<Integer> path = getPath(0, i);
        int bottom = 1;
        int top = path.size() - 1;
        E elem = (E) tempValues[top];
        E root = (E) es[i];

        for(int  idx = path.size() - 1; idx > 0; idx--) {
            int pathIdx = path.get(idx);

            if(((Comparable<? super E>) elem).compareTo(root) > 0)  {
                es[pathIdx] = root;
                siftDown(i, (E) tempValues[bottom++]);
                root = (E) es[i];
            } else {
                es[pathIdx] = (E) tempValues[top--];
                elem = (E) tempValues[top];
            }
        }
    }

    private void WalkDownUsingComparator(int i, Object[] es, Comparator<? super E> cmp) {
        List<Integer> path = getPath(0, i);
        int bottom = 1;
        int top = path.size() - 1;
        E elem = (E) tempValues[top];
        E root = (E) es[i];

        for(int  idx = path.size() - 1; idx > 0; idx--) {
            int pathIdx = path.get(idx);

            if(cmp.compare(elem, root) <= 0) {
                es[pathIdx] = root;
                siftDown(i, (E) tempValues[bottom++]);
                root = (E) es[i];
            } else {
                es[pathIdx] = (E) tempValues[top--];
                elem = (E) tempValues[top];
            }
        }
    }

    /**
     * Returns the (reversed) path between the two input indices and fills in the values in tempValues
     * @param from
     * @param to
     * @return
     */
    private List<Integer> getPath(int from, int to) {
        ArrayList<Integer> path = new ArrayList<>();
        int i;
        path.add(to);
        tempValues[(i = 0)] = queue[to];
        while(to != from){
            path.add(to = (to - 1) >>> 1);
            tempValues[++i] = queue[to];
        }
        return path;
    }
}

