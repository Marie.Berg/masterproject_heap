package heap;

import java.util.Collection;

/**
 * A priority queue similar to {@link PriorityQueue} where the {@code addAll} method adds the element to the end of the
 * heap ina bulk manner and then reconstucts the heap by calling the {@code heapify} method.
 * @param <E>
 */
public class BulkInsertionHeapify<E> extends PriorityQueue<E> {

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int k = c.size();
        if(size > queue.length - k)
            grow(size + k + 1);
        System.arraycopy(c.toArray(), 0, queue, size(), c.size());
        size += c.size();
        heapify();
        return true;
    }
}
