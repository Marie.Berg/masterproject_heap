import heap.BulkInsertionHeapify;
import heap.BulkInsertionWalkDown;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Main {

    static int numIterations = 100;         //Number of iterations
    static int n = (int) Math.pow(2, 24);   //Size of initial data
    static int k = (n * 10) / 100;          //Size of input data

    static boolean runWithTests = true;    //Run testcases with testing or not

    static File SequentialResultFile = new File("PriorityQueueResults.txt");
    static File walkDownResultFile = new File("WalkDownResults.txt");
    static File heapifyResultFile = new File("HeapifyResults.txt");

    static FileWriter sequentialFileWriter;
    static FileWriter WalkDownFileWriter;
    static FileWriter heapifyFileWriter;

    static {
        try {
            WalkDownFileWriter = new FileWriter(walkDownResultFile);
            sequentialFileWriter = new FileWriter(SequentialResultFile);
            heapifyFileWriter = new FileWriter(heapifyResultFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retuns a list of random integers
     * @param size
     * @return
     */
    public static ArrayList<Integer> getRandomIntegerDataset(int size) {
        Random random = new Random();
        ArrayList<Integer> data = new ArrayList<>(size);
        for(int i = 0; i < size; i++)
            data.add(random.nextInt());
        return data;
    }

    /**
     * Returns a list of positive random integers.
     * @param size size of the returned list
     * @return
     */
    public static ArrayList<Integer> getRandomPositiveDataset(int size) {
        Random random = new Random();
        ArrayList<Integer> data = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            int val = random.nextInt(Integer.MAX_VALUE);
            data.add(val);
        }
        return data;
    }

    /**
     * Returns a list of negative random integers
     * @param size size of the returned list
     * @return
     */
    public static ArrayList<Integer> getRandomNegativeIntegerDataset(int size) {
        Random random = new Random();
        ArrayList<Integer> data = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            int val = random.nextInt(Integer.MAX_VALUE);
            data.add(-1 * val);
        }
        return data;
    }

    /**
     * Take the time and save the time spend on the addAll procedure
     * @param queue
     * @param data
     * @param file
     * @return
     * @throws IOException
     */
    public static long runAndSaveToFile(Queue<Integer> queue, ArrayList<Integer> data, FileWriter file) throws IOException {
        long start = System.nanoTime();
        queue.addAll(data);
        long end = System.nanoTime();
        file.write(end - start + ","+ "\n");
        return  end - start;
    }

    /**
     * Testcase with random input data and random initial data
     * @throws IOException
     */
    public static void randomTestcase() throws IOException {
        System.out.println("Running random testcase");
        //Accumulated times
        long SequentialAcc = 0;
        long walkDownAcc = 0;
        long heapifyAcc = 0;

        //Generating initial data
        ArrayList<Integer> initData = getRandomIntegerDataset(n);

        for(int i = 0; i < numIterations; i++) {
            //Initializing heaps
            PriorityQueue<Integer> sequentialInsertionHeap = new PriorityQueue<>();
            BulkInsertionWalkDown<Integer> bulkInsertionWalkDownHeap = new BulkInsertionWalkDown();
            BulkInsertionHeapify<Integer> bulkInsertionHeapifyHeap = new BulkInsertionHeapify<>();

            //Adding initial data
            for(int elem : initData) {
                sequentialInsertionHeap.add(elem);
                bulkInsertionWalkDownHeap.add(elem);
                bulkInsertionHeapifyHeap.add(elem);
            }

            //Generating input data
            ArrayList<Integer> data = getRandomIntegerDataset(k);

            //Timing the addAll procedure
            SequentialAcc += runAndSaveToFile(sequentialInsertionHeap, data, sequentialFileWriter);
            walkDownAcc += runAndSaveToFile(bulkInsertionWalkDownHeap, data, WalkDownFileWriter);
            heapifyAcc += runAndSaveToFile(bulkInsertionHeapifyHeap, data, heapifyFileWriter);

            //Testing
            if(runWithTests) {
                for (int j = 0; j < n + k; j++) {
                    if (!sequentialInsertionHeap.poll().equals(bulkInsertionWalkDownHeap.poll())) {
                        System.out.println("Something is wrong!");
                        return;
                    }
                }
            }
            System.out.println("i: " + i);
        }
        sequentialFileWriter.close();
        WalkDownFileWriter.close();
        heapifyFileWriter.close();

        //Printing accumulated times
        System.out.println("Done");
        System.out.println("Sequential insertion: " + SequentialAcc);
        System.out.println("Bulk insertion walk down: " + walkDownAcc);
        System.out.println("Bulk insertion heapify: " + heapifyAcc);
    }

    /**
     * Testcase where all elements of the input data is smaller than the initial data
     * @throws IOException
     */
    public static void worstCaseTestcase() throws IOException {
        System.out.println("Runnning worst-case testset");

        //Accumulated times
        long sequentialAcc = 0;
        long walkDownAcc = 0;
        long heapifyAcc = 0;

        //Generating intial data
        ArrayList<Integer> initData = getRandomPositiveDataset(n);

        for(int i = 0; i < numIterations; i++) {
            //Initializing heaps
            PriorityQueue<Integer> sequentialInsertionHeap = new PriorityQueue<>();
            BulkInsertionWalkDown<Integer> bulkInsertionWalkDownHeap = new BulkInsertionWalkDown();
            BulkInsertionHeapify<Integer> bulkInsertionHeapifyHeap = new BulkInsertionHeapify<>();

            //Adding initial data
            for(int elem : initData) {
                sequentialInsertionHeap.add(elem);
                bulkInsertionWalkDownHeap.add(elem);
                bulkInsertionHeapifyHeap.add(elem);
            }

            //Generating input data
            ArrayList<Integer> data = getRandomNegativeIntegerDataset(k);
            Collections.sort(data);
            Collections.reverse(data);

            //Timing the addAll procedure
            sequentialAcc += runAndSaveToFile(sequentialInsertionHeap, data, sequentialFileWriter);
            walkDownAcc += runAndSaveToFile(bulkInsertionWalkDownHeap, data, WalkDownFileWriter);
            heapifyAcc += runAndSaveToFile(bulkInsertionHeapifyHeap, data, heapifyFileWriter);

            //Testing
            if(runWithTests) {
                for (int j = 0; j < n + k; j++) {
                    if (!sequentialInsertionHeap.poll().equals(bulkInsertionWalkDownHeap.poll())) {
                        System.out.println("Something is wrong!");
                        return;
                    }
                }
            }
            System.out.println("i: " + i);
        }

        //Closing files
        sequentialFileWriter.close();
        WalkDownFileWriter.close();
        heapifyFileWriter.close();

        //Printing accumulated times
        System.out.println("Done");
        System.out.println("pq: " + sequentialAcc);
        System.out.println("wh: " + walkDownAcc);
        System.out.println("heapify: " + heapifyAcc);
    }

    public static void main(String[] args) throws IOException {
        worstCaseTestcase();
        //randomTestcase();
    }

}
